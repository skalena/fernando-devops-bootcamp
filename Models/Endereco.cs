    public class Endereco
    {
        public string bairro { get; set; }
        public string cep { get; set; }
        public string cidade { get; set; }
        public string complemento2 { get; set; }
        public string end { get; set; }
        public string uf { get; set; }
    }