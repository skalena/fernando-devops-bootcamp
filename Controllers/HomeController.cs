﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using api_correios.Models;
using api_correios.Repositories;
using api_correios.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ServiceReference;

namespace api_correios.Controllers
{
    [ApiController]
    [Route("consultas")]
    [Produces("application/json")]
    public class HomeController : ControllerBase
    {

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }        

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Autenticar([FromBody] User model)
        {
            var user = UserRepository.Get(model.Username, model.Password);

            if (user == null)
            {
                return NotFound(new { message = "Usuário ou senha invalido!" });
            }

            var token = TokenService.GenerateToken(user);
            user.Password = "";

            return new User()
            {
                Username = user.Username,
                Role = user.Role,
                Token = token
            };
        }

        [HttpPost]
        [Authorize]
        [Route("cepAuth")]
        public async Task<ActionResult> ConsultarCepPost([FromBody]Cep cep){

            try
            {
                var _cep = System.Text.Json.JsonSerializer.Serialize(cep);

                var cepSoap = new AtendeClienteClient();
                var resultado = await cepSoap.consultaCEPAsync(_cep.ToString());

                Endereco er = new Endereco() {
                    bairro = resultado.@return.bairro,
                    cep = resultado.@return.cep,
                    cidade = resultado.@return.cidade,
                    complemento2 = resultado.@return.complemento2,
                    end = resultado.@return.end,
                    uf = resultado.@return.uf
                };

                return Ok(er);
            }
            catch (System.Exception e)
            {
                return BadRequest( new {
                    status = "ERROR",
                    description = e.Message,
                    trace = e.StackTrace
                });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("cep/{cep:minlength(8):maxlength(8)}")]
        public async Task<ActionResult> ConsultarCep(string cep){

            try
            {
                var cepSoap = new AtendeClienteClient();
                var resultado = await cepSoap.consultaCEPAsync(cep);

                Endereco er = new Endereco() {
                    bairro = resultado.@return.bairro,
                    cep = resultado.@return.cep,
                    cidade = resultado.@return.cidade,
                    complemento2 = resultado.@return.complemento2,
                    end = resultado.@return.end,
                    uf = resultado.@return.uf
                };

                return Ok(er);
            }
            catch (System.Exception e)
            {
                return BadRequest( new {
                    status = "ERROR",
                    description = e.Message,
                    trace = e.StackTrace
                });
            }

        }
    }
}
